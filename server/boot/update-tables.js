module.exports = function(app) {
    var mysqlDs = app.dataSources.mysqlDs;
    // var Author = app.models.Author;
    
    // first autoupdate the `Author` model to avoid foreign key constraint failure
    var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role' , 'viajes'];
    mysqlDs.automigrate(lbTables, function(er) {
      if (er) throw er;
      console.log('Loopback tables [' + lbTables + '] created in ', mysqlDs.adapter.name);
      mysqlDs.disconnect();
    });
  };